import scr.SensorModel;
import org.encog.neural.networks.BasicNetwork;
import org.encog.ml.data.basic.BasicMLData;
import java.io.*;
import org.encog.persist.EncogDirectoryPersistence;

public class NeuralNetwork implements Serializable {

    private static final long serialVersionUID = -88L;
    private BasicNetwork network;

    NeuralNetwork() {
        loadGenome();
    }

    public double getOutput(SensorModel a,String key) {
        
        double trackPosition = a.getTrackPosition();
        double trackAngle = a.getAngleToTrackAxis();
        double speed = a.getSpeed();
        double[] trackEdges = a.getTrackEdgeSensors();
        
        int outerLayer = 4; //laatste layer 

        double[] input = new double[22];
        input[0] = speed;
        input[1] = trackPosition;
        input[2] = trackAngle;
        for (int i = 3; i < input.length; i++) {
            input[i] = trackEdges[i - 3];
        }
        BasicMLData nn_input = new BasicMLData(input);

        network.compute(nn_input);
        
        switch (key) {
            case "acceleration":
                return network.getLayerOutput(outerLayer, 0);
            case "brake":
                return network.getLayerOutput(outerLayer, 1);
            case "steering":
                return network.getLayerOutput(outerLayer, 2);
            default:
                break;
        }
        return 0.5;      
    }


    // Load a neural network from memory
    public BasicNetwork loadGenome() {
           
        File file = new File("./src/netwerk/nouriG1V3.nouri01"); //network file #nouri
       
        try{
            network=(BasicNetwork) EncogDirectoryPersistence.loadObject(file);
            return network;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
