import pandas as pd
import numpy as np

df = pd.read_csv("Corners.csv",index_col=False )

cols={'ACCELERATION':[0,1],'BRAKE':[0,1],'STEERING':[-1,1],'ANGLE_TO_TRACK_AXIS':[-np.pi,np.pi],
'TRACK_EDGE_0':[0,200],'TRACK_EDGE_1':[0,200],'TRACK_EDGE_2':[0,200],'TRACK_EDGE_3':[0,200],'TRACK_EDGE_4':[0,200],'TRACK_EDGE_5':[0,200]
,'TRACK_EDGE_6':[0,200],'TRACK_EDGE_7':[0,200],'TRACK_EDGE_8':[0,200],'TRACK_EDGE_9':[0,200],'TRACK_EDGE_10':[0,200],'TRACK_EDGE_11':[0,200]
,'TRACK_EDGE_12':[0,200],'TRACK_EDGE_13':[0,200],'TRACK_EDGE_14':[0,200],'TRACK_EDGE_15':[0,200],'TRACK_EDGE_16':[0,200],'TRACK_EDGE_17':[0,200]
,'TRACK_EDGE_18':[0,200]}
#print(type(df['ACCELERATION'][66]))

for col in cols:
    print(col)
    cnt = 0
    for row in range(0,df[col].size):
        # print()
        if df[col][row] < cols[col][0] and df[col][row] > cols[col][1]:
            print(df[col][row])
            cnt+=1
            # pass
    print("aantal: %s"%cnt)
     #pass
